package com.example.coroutinesapi.network.web_response

import com.example.coroutinesapi.models.CategoriesModel

class GetCategoriesResponse : BaseResponse() {
    var data =  Data()

    inner class Data {
        var categories: ArrayList<CategoriesModel>? = null
    }
}