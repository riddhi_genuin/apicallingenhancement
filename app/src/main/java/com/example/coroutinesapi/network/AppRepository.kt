package com.example.coroutinesapi.network

import com.example.coroutinesapi.network.web_request.RequestBodies
import com.example.coroutinesapi.utils.APIConstants

class AppRepository(private var token: String) {
    init {
        //var token = shar
    }

    suspend fun addQuestion(body: RequestBodies.AddCustomQuestion) = RetrofitInstance.retrofitService.addQuestion(token, APIConstants.ADD_QUESTION, body)

    suspend fun getCategories() = RetrofitInstance.retrofitService.getCategories(token, APIConstants.GET_CATEGORIES)
}