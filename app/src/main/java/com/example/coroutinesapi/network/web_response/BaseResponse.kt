package com.example.coroutinesapi.network.web_response

open class BaseResponse {
    var code: Int? = null
    var message: String? = null
}