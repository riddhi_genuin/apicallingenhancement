package com.example.coroutinesapi.network

import com.example.coroutinesapi.network.web_request.RequestBodies
import com.example.coroutinesapi.network.web_response.AddCustomQuestionResponse
import com.example.coroutinesapi.network.web_response.GetCategoriesResponse
import retrofit2.Response
import retrofit2.http.*

interface API {

    @POST("{module}")
    suspend fun addQuestion(
        @Header("x-auth-token") token: String,
        @Path(value = "module", encoded = true) module: String,
        @Body body: RequestBodies.AddCustomQuestion,
    ): Response<AddCustomQuestionResponse>

    @GET("{module}")
    suspend fun getCategories(
        @Header("x-auth-token") token: String,
        @Path(value = "module", encoded = true) module: String,
    ): Response<GetCategoriesResponse>
}