package com.example.coroutinesapi.network.web_request

object RequestBodies {

    data class AddCustomQuestion(
        val question:String
    )

}