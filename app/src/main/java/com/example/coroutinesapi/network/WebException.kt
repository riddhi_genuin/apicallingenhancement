package com.example.coroutinesapi.network

class WebException {
    var code: Int? = null
    var message: String? = null
}