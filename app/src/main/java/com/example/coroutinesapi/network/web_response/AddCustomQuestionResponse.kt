package com.example.coroutinesapi.network.web_response

import com.example.coroutinesapi.models.QuestionModel

class AddCustomQuestionResponse : BaseResponse() {

    var data: QuestionModel? = null
}