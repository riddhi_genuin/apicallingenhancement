package com.example.coroutinesapi

import android.app.Application
import android.content.Context

class MyApplication : Application() {
    //private var context: Context? = null


    override fun onCreate() {
        super.onCreate()
        instance = this
        //this.context = this

    }

    /* fun getContext(): Context? {
         return context
     }*/

    companion object {
        private var instance: MyApplication? = null
        fun getInstance(): MyApplication? {
            if (instance == null) {
                instance = MyApplication()
            }
            return instance
        }
    }


}