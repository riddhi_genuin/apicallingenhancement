package com.example.coroutinesapi.models

data class CategoriesModel(var uuid: String, var category: String)
