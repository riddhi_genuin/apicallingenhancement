package com.example.coroutinesapi.models

data class QuestionModel(var id: Int = -1, var question: String = "") {

    override fun toString(): String {
        return "ID: $id text: $question"
    }
}