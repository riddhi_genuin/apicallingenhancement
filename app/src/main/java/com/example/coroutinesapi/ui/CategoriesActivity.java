package com.example.coroutinesapi.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.example.coroutinesapi.R;
import com.example.coroutinesapi.adapter.CategoriesAdapter;
import com.example.coroutinesapi.models.CategoriesModel;
import com.example.coroutinesapi.network.AppRepository;
import com.example.coroutinesapi.network.WebException;
import com.example.coroutinesapi.network.web_response.GetCategoriesResponse;
import com.example.coroutinesapi.utils.APICallBack;
import com.example.coroutinesapi.utils.Utils;
import com.example.coroutinesapi.viewmodels.GetCategoriesViewModel;
import com.example.coroutinesapi.viewmodels.ViewModelProviderFactory;

import java.util.ArrayList;

import okhttp3.internal.Util;

public class CategoriesActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private CategoriesAdapter adapter;
    private GetCategoriesViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        init();

    }

    private void init() {
        //AppRepository appRepository = new AppRepository(Utils.INSTANCE.getToken());
        //viewModel = new GetCategoriesViewModel(appRepository);
        initViewModel();

        recyclerView = findViewById(R.id.rvCategories);
        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);

        adapter = new CategoriesAdapter(null);
        recyclerView.setAdapter(adapter);

        loadCategoryData();
    }

    private void initViewModel(){
        AppRepository appRepository = new AppRepository(Utils.INSTANCE.getToken());
        ViewModelProviderFactory factory = new ViewModelProviderFactory(appRepository);
        viewModel = new ViewModelProvider(this,factory).get(GetCategoriesViewModel.class);
    }

    private void loadCategoryData() {
        viewModel.loadCategories(this, new APICallBack() {
            @Override
            public void onLoading() {
                Utils.INSTANCE.showProgressDialog(CategoriesActivity.this);
            }

            @Override
            public void onSuccess(@NonNull Object any) {
                ArrayList<CategoriesModel> list = (ArrayList<CategoriesModel>) any;
                Log.e("body",any.toString());
                adapter.updateList(list);
                Utils.INSTANCE.hideProgressDialog();
            }

            @Override
            public void onError(@NonNull WebException exception) {
                Utils.INSTANCE.showMessage(CategoriesActivity.this, TextUtils.isEmpty(exception.getMessage()) ? "Something went wrong" : exception.getMessage());
                Utils.INSTANCE.hideProgressDialog();
            }

            @Override
            public void onNoInternetFound(@NonNull String message) {
                Utils.INSTANCE.showMessage(CategoriesActivity.this, message);
            }
        });
    }
}