package com.example.coroutinesapi.ui

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.coroutinesapi.models.QuestionModel
import com.example.coroutinesapi.R
import com.example.coroutinesapi.network.AppRepository
import com.example.coroutinesapi.network.WebException
import com.example.coroutinesapi.network.web_request.RequestBodies
import com.example.coroutinesapi.network.web_response.AddCustomQuestionResponse
import com.example.coroutinesapi.utils.APICallBack
import com.example.coroutinesapi.utils.Utils
import com.example.coroutinesapi.viewmodels.AddQuestionViewModel
import com.example.coroutinesapi.viewmodels.ViewModelProviderFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.include_add_question.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private var oldQuestionText: String? = null
    private var newCustomQuestionModel: QuestionModel? = null
    lateinit var addQuestionViewModel: AddQuestionViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViewModel()

        btnOpenDialog.setOnClickListener {
            //openAddCustomQuestionView();
            generateTempQuestionModel(edtAddQuestion.text.toString())
            onSlideUp()
        }

        btnOpenCategory.setOnClickListener {
            startActivity(Intent(this, CategoriesActivity::class.java))
        }
        tvDone.setOnClickListener {
            callAPIFromHere();

        }

        edtAddQuestion.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                edtAddQuestion.clearComposingText()
            }

            override fun afterTextChanged(s: Editable) {
                var charLength = 0
                var isEnableNeed = false
                if (s.isNotEmpty()) {
                    isEnableNeed = true
                    tvDone.isEnabled = true
                    charLength = s.length
                    edtAddQuestion.setSelection(s.length)
                }
                setDoneButton(isEnableNeed)
                tvCharCount.text = "${charLength}/75"
            }
        })
    }

    private fun initViewModel() {
        val repository = AppRepository(Utils.getToken())
        val factory = ViewModelProviderFactory(repository)
        addQuestionViewModel = ViewModelProvider(this, factory)[AddQuestionViewModel::class.java]
    }

    private fun callAPIFromHere() {
        Utils.hideKeyboard(this)
        rlAddQuestion.visibility = View.GONE
        tvAddedQuestion.visibility = View.VISIBLE
        llTop.visibility = View.VISIBLE
        val request = RequestBodies.AddCustomQuestion(edtAddQuestion.text.toString())
        GlobalScope.launch {
            addQuestionViewModel.addCustomQuestion(request, object : APICallBack {
                override fun onLoading() {
                    Utils.showProgressDialog(this@MainActivity)
                }

                override fun onSuccess(any: Any) {
                    val finalResponse = any as AddCustomQuestionResponse
                    if (finalResponse.code == 200) {
                        if (finalResponse.data != null) {
                            newCustomQuestionModel = finalResponse.data
                        }
                        generateTempQuestionModel(edtAddQuestion.text.toString())
                        //llAddCustomQuestion.visibility = View.VISIBLE
                        //rlAddQuestion
                    } else {
                        Utils.showMessage(this@MainActivity, "Response is null")
                    }
                }

                override fun onError(exception: WebException) {
                    Utils.showMessage(this@MainActivity, exception.message!!)
                }

                override fun onNoInternetFound(message: String) {
                    Utils.showMessage(this@MainActivity, message)
                }
            })
        }

    }


    private fun onSlideUp() {
        rlAddQuestion.visibility = View.VISIBLE
        edtAddQuestion.requestFocus()
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
        openAddCustomQuestionView()
        llTop.visibility = View.GONE
    }

    private fun openAddCustomQuestionView() {
        val isQuestionModelAvailable = !TextUtils.isEmpty(newCustomQuestionModel?.question)
        //customAddQuestionView.setCustomQuestionData(newCustomQuestionModel, edtAddQuestion.text.toString().trim { it <= ' ' }.length)
        tvCustomQuestionHeader.text =
            if (isQuestionModelAvailable) "Edit Question" else "Add a Question"
        setDoneButton(isQuestionModelAvailable)
    }

    private fun setDoneButton(isEnable: Boolean) {
        tvDone.isEnabled = isEnable
        tvDone.setTextColor(if (isEnable) getColor(R.color.colorWhite) else getColor(R.color.white_opacity40))
        tvDone.text = if (TextUtils.isEmpty(newCustomQuestionModel?.question)) "Done" else "Edit"
    }

    private fun generateTempQuestionModel(questionText: String) {
        if (newCustomQuestionModel == null) {
            newCustomQuestionModel = QuestionModel()
        }
        newCustomQuestionModel?.question = (questionText)
        newCustomQuestionModel?.id = 1
        oldQuestionText = newCustomQuestionModel?.question
    }
}