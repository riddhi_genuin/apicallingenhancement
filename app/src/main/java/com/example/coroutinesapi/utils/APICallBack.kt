package com.example.coroutinesapi.utils

import com.example.coroutinesapi.network.WebException

interface APICallBack {
    fun onLoading()
    fun onSuccess(any: Any)
    fun onError(exception: WebException)
    fun onNoInternetFound(message:String)
}