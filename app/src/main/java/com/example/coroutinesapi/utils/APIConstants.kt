package com.example.coroutinesapi.utils

object APIConstants {
    const val ADD_QUESTION = "question/create"
    const val GET_CATEGORIES = "categories"
}