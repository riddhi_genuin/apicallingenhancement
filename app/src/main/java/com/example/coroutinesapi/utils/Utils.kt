package com.example.coroutinesapi.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.example.coroutinesapi.R
import com.example.coroutinesapi.network.WebException

object Utils {
    private var progressDialog: Dialog? = null
    
    fun isNetworkAvailable(context: Context?): Boolean {
        if (context == null) return false
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                return when {
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                        true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                        true
                    }
                    else -> capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
                }
            }
        } else {
            try {
                val activeNetworkInfo = connectivityManager.activeNetworkInfo
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                    return true
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return false
    }

    fun hideKeyboard(activity: Activity) {
        try {
            if (activity.currentFocus != null) {
                val imm =
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    fun generateWebException(code: Int, message: String): WebException {
        val webException = WebException()
        webException.code = code
        webException.message = message
        return webException
    }

    fun getToken(): String {
        return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiYWYzZjZiZDktZWM1NC00YjFlLWIyOWUtMjIzOGVmMmNjMTNlIiwiZmlsdGVyX2Rpc3RhbmNlIjoxMCwibmFtZSI6IlJpZGRoaSBTaGFoIiwibmlja25hbWUiOiJyaWRkaGlzaGFoIiwicGhvbmUiOiI5MTk5MjUwMDkyODUiLCJiaW8iOiJTci4gQW5kcm9pZCBEZXZlbG9wZXIgYXQgR2VudWluIiwiaXNfbmV3X21lc3NhZ2UiOmZhbHNlLCJpc19hdmF0YXIiOnRydWUsImxvY2F0aW9ucyI6W10sImNhdGVnb3JpZXMiOltdLCJwcm9maWxlX2ltYWdlIjoic21pbGluZ19mYWNlX3dpdGhfc3VuZ2xhc3NlcyIsInByb2ZpbGVfaW1hZ2VfcyI6IiIsInByb2ZpbGVfaW1hZ2VfbSI6IiIsInByb2ZpbGVfaW1hZ2VfbCI6IiIsImJpcnRoZGF5IjoiMTAvMDgvMTk4OSIsImZpbHRlcl9nZW5kZXJfcmFuZ2UiOiJmZW1hbGUsY291cGxlIiwiZ2VuZGVyIjoibWFsZSIsImlhdCI6MTY0NTUzNDI5NSwiZXhwIjoxNjQ1NjIwNjk1fQ.LIzRSeW5s2oAxng1Fj4BafGXGaJgHr-cQkpjlFT1F6I"
    }
    fun showProgressDialog(activity: Activity) {
        try {
            if (progressDialog != null) {
                return
            }
            if (activity == null) {
                return
            }
            progressDialog = Dialog(activity)
            progressDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            progressDialog?.setContentView(R.layout.progress_dialog)
            progressDialog?.setCancelable(false)
            progressDialog?.setCanceledOnTouchOutside(false)
            if (progressDialog?.window != null) {
                val lp: WindowManager.LayoutParams = progressDialog?.window?.attributes!!
                lp.dimAmount = 0.6f // Dim level. 0.0 - no dim, 1.0 - completely opaque
                progressDialog?.window?.attributes = lp
            }
            if (!activity.isFinishing) {
                progressDialog?.show()
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    fun hideProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog?.dismiss()
                progressDialog = null
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

     fun showMessage(activity: Activity, message: String) {
         activity.runOnUiThread {
             Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
         }

    }
}