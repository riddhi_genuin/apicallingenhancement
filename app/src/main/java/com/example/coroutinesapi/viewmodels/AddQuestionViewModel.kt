package com.example.coroutinesapi.viewmodels

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.coroutinesapi.MyApplication
import com.example.coroutinesapi.R
import com.example.coroutinesapi.models.QuestionModel
import com.example.coroutinesapi.network.AppRepository
import com.example.coroutinesapi.network.WebException
import com.example.coroutinesapi.utils.Utils
import com.example.coroutinesapi.network.web_request.RequestBodies
import com.example.coroutinesapi.utils.APICallBack
import kotlinx.coroutines.launch
import kotlinx.coroutines.yield
import java.lang.Exception

class AddQuestionViewModel(private var appRepository: AppRepository) : ViewModel() {

    suspend fun addCustomQuestion(body: RequestBodies.AddCustomQuestion, callBack: APICallBack) {
        if (Utils.isNetworkAvailable(MyApplication.getInstance()?.applicationContext)) {
            callBack.onLoading()
            viewModelScope.launch {
                try {
                    yield()
                    val response = appRepository.addQuestion(body)
                    if (response.isSuccessful) {
                        response.body()?.let {
                            Log.e("body", response.body().toString())
                            callBack.onSuccess(it)
                        }
                    } else {
                        val exception = WebException()
                        exception.code = response.code()
                        exception.message = response.message()
                        callBack.onError(exception)
                    }

                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        } else {
            callBack.onNoInternetFound(MyApplication.getInstance()
                ?.getString(R.string.no_internet_found)!!)
        }
    }

}