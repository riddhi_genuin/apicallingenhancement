package com.example.coroutinesapi.viewmodels

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.coroutinesapi.R
import com.example.coroutinesapi.network.AppRepository
import com.example.coroutinesapi.network.web_response.GetCategoriesResponse
import com.example.coroutinesapi.utils.APICallBack
import com.example.coroutinesapi.utils.Utils
import com.google.gson.Gson
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class GetCategoriesViewModel(private var appRepository: AppRepository) : ViewModel() {

    /* fun loadCategories(context: Context, apiCallBack: APICallBack) = viewModelScope.launch {
         getCategories(context, apiCallBack)
     }*/

    fun loadCategories(context: Context, apiCallBack: APICallBack) {
        if (!Utils.isNetworkAvailable(context)) {
            apiCallBack.onNoInternetFound(context.getString(R.string.no_internet_found))
            return
        }
        val response = viewModelScope.async {
            getCategories(context, apiCallBack)
        }
    }

    private suspend fun getCategories(context: Context, apiCallBack: APICallBack) {
        try {
            if (Utils.isNetworkAvailable(context)) {
                apiCallBack.onLoading()
                val response = appRepository.getCategories()
                if (response.isSuccessful && response.body() != null) {
                    //gson.fromJson(response.body(), GetCategoriesResponse::class.java)
                    if (response.isSuccessful) {
                        if (response.body() != null && response.body()?.data != null) {
                            response.body()?.data?.categories?.let { apiCallBack.onSuccess(it) }
                        } else {
                            var message = response.message()
                            if (response.message().isEmpty()) message = "No data found."
                            apiCallBack.onError(Utils.generateWebException(response.code(),
                                message))
                        }
                    } else {
                        apiCallBack.onError(Utils.generateWebException(response.code(),
                            response.message()))
                    }
                }
            } else {
                apiCallBack.onNoInternetFound(context.getString(R.string.no_internet_found))
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
            apiCallBack.onError(Utils.generateWebException(-101, ex.printStackTrace().toString()))
        }
    }
}