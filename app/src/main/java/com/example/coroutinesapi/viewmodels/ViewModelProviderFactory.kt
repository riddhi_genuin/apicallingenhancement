package com.example.coroutinesapi.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.coroutinesapi.network.AppRepository

class ViewModelProviderFactory(private val appRepository: AppRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AddQuestionViewModel::class.java)) {
            return AddQuestionViewModel(appRepository) as T
        }
        if (modelClass.isAssignableFrom(GetCategoriesViewModel::class.java)) {
            return GetCategoriesViewModel(appRepository) as T
        }

        throw IllegalArgumentException("Unknown class name")
    }


}