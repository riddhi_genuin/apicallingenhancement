package com.example.coroutinesapi.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.coroutinesapi.R;
import com.example.coroutinesapi.models.CategoriesModel;

import java.util.ArrayList;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoryViewHolder> {
    private ArrayList<CategoriesModel> categoriesList;

    public CategoriesAdapter(ArrayList<CategoriesModel> list) {
        this.categoriesList = list;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CategoryViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.categories_item,
                parent,
                false));
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        holder.setData(categoriesList.get(position));
        if (position == getItemCount()) {
            holder.view.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return categoriesList == null || categoriesList.size() == 0 ? 0 : categoriesList.size();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCategoryId, tvCategoryName;
        private View view;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCategoryId = itemView.findViewById(R.id.tvCategoryId);
            tvCategoryName = itemView.findViewById(R.id.tvCategoryName);
            view = itemView.findViewById(R.id.view);
        }

        private void setData(CategoriesModel model) {
            tvCategoryName.setText(model.getCategory());
            tvCategoryId.setText(model.getUuid());
        }

    }

    public void updateList(ArrayList<CategoriesModel> categoriesList) {
        this.categoriesList = categoriesList;
        notifyDataSetChanged();
    }
}
